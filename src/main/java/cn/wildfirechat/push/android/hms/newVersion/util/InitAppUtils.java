/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2024. All rights reserved.
 */
package cn.wildfirechat.push.android.hms.newVersion.util;


import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiApp;
import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiCredential;
import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiOption;

public class InitAppUtils {
    /**
     * @return HuaweiApp
     */
//    public static HuaweiApp initializeApp(String ) {
//        String appId = ResourceBundle.getBundle("url").getString("appid");
//        String appSecret = ResourceBundle.getBundle("url").getString("appsecret");
//        // Create HuaweiCredential
//        // This appId and appSecret come from Huawei Developer Alliance
//        return initializeApp(appId, appSecret);
//    }

    public static HuaweiApp initializeApp(String appId, String appSecret) {
        HuaweiCredential credential = HuaweiCredential.builder()
                .setAppId(appId)
                .setAppSecret(appSecret)
                .build();

        // Create HuaweiOption
        HuaweiOption option = HuaweiOption.builder()
                .setCredential(credential)
                .build();

        // Initialize HuaweiApp
//        return HuaweiApp.initializeApp(option);
        return HuaweiApp.getInstance(option);
    }
}
