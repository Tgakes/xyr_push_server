/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package cn.wildfirechat.push.android.hms.newVersion;


import cn.wildfirechat.push.PushMessage;
import cn.wildfirechat.push.android.hms.HMSPush;
import cn.wildfirechat.push.android.hms.newVersion.exception.HuaweiMesssagingException;
import cn.wildfirechat.push.android.hms.newVersion.message.AndroidConfig;
import cn.wildfirechat.push.android.hms.newVersion.message.Message;
import cn.wildfirechat.push.android.hms.newVersion.message.Notification;
import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiApp;
import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiMessaging;
import cn.wildfirechat.push.android.hms.newVersion.model.Urgency;
import cn.wildfirechat.push.android.hms.newVersion.reponse.SendResponse;
import cn.wildfirechat.push.android.hms.newVersion.util.InitAppUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

public class SendDataMessage2 {


    /**
     * send data message
     *
     * @throws HuaweiMesssagingException
     */
    public void sendTransparent(String appId, String appSecret, PushMessage pushMessage) throws HuaweiMesssagingException {
        HuaweiApp app = InitAppUtils.initializeApp(appId, appSecret);
        HuaweiMessaging huaweiMessaging = HuaweiMessaging.getInstance(app);

        AndroidConfig androidConfig = AndroidConfig.builder().setCollapseKey(-1)//-1：对所有离线消息都缓存。
//                .setCategory("VOIP")//VoIP电话
                .setUrgency(Urgency.HIGH.getValue())
                .setTtl("10000s")
                .setBiTag("the_sample_bi_tag_for_receipt_service")
                .build();

//        String token = "AND8rUp4etqJvbakK7qQoCVgFHnROXzH8o7B8fTl9rMP5VRFN83zU3Nvmabm3xw7e3gZjyBbp_wfO1jP-UyDQcZN_CtjBpoa7nx1WaVFe_3mqXMJ6nXJNUZcDyO_-k3sSw";
        String data = JSONObject.toJSONString(pushMessage);
//        Notification notification = Notification.builder()
//                .setTitle(StringUtils.isEmpty(pushMessage.senderName) ? "音视频消息" : pushMessage.senderName)
//                .setBody(StringUtils.isEmpty(pushMessage.pushContent) ? data: pushMessage.pushContent)
//                .build();
        Message message = Message.builder()
                .setData(data)
                .setAndroidConfig(androidConfig)
//                .setNotification(notification)
                .addToken(pushMessage.deviceToken)
                .build();



        SendResponse response = huaweiMessaging.sendMessage(message);
        HMSPush.LOG.info("透传消息推送response:", response.toString());
    }
}
