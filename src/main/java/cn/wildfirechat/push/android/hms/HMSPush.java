package cn.wildfirechat.push.android.hms;


import cn.wildfirechat.push.PushMessage;
import cn.wildfirechat.push.PushMessageType;
import cn.wildfirechat.push.android.hms.newVersion.SendDataMessage;
import cn.wildfirechat.push.android.hms.newVersion.SendNotifyMessage;
import cn.wildfirechat.push.android.hms.newVersion.exception.HuaweiMesssagingException;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.List;

@Component
public class HMSPush {
    public static final Logger LOG = LoggerFactory.getLogger(HMSPush.class);

    @Autowired
    private HMSConfig mConfig;

    SendNotifyMessage sendNotifyMessage;

    SendDataMessage sendDataMessage;


    //发送Push消息
    public void push(PushMessage pushMessage) {
        boolean isNotice = !(pushMessage.pushMessageType != PushMessageType.PUSH_MESSAGE_TYPE_NORMAL && pushMessage.pushMessageType != PushMessageType.PUSH_MESSAGE_TYPE_FRIEND_REQUEST);
        String msgBody = MessageFormat.format("AppSecret={0},AppId={1}", mConfig.getAppSecret(), mConfig.getAppId());
        LOG.info("华为配置信息:" + msgBody);
        if (isNotice) {
            if (sendNotifyMessage == null) {
                sendNotifyMessage = new SendNotifyMessage();
            }
            try {
                sendNotifyMessage.sendNotification(mConfig.getAppId(), mConfig.getAppSecret(), pushMessage);
            } catch (HuaweiMesssagingException e) {
                LOG.info("通知栏推送异常:" + e.getMessage());
            }
        } else {
            if (sendDataMessage == null) {
                sendDataMessage = new SendDataMessage();
            }

            try {
                sendDataMessage.sendTransparent(mConfig.getAppId(), mConfig.getAppSecret(), pushMessage);
            } catch (HuaweiMesssagingException e) {
                LOG.info("透传推送异常:" + e.getMessage());
            }
        }
    }


}
