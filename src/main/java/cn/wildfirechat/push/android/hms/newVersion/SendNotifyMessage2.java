/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package cn.wildfirechat.push.android.hms.newVersion;

import cn.wildfirechat.push.PushMessage;
import cn.wildfirechat.push.android.hms.HMSPush;
import cn.wildfirechat.push.android.hms.newVersion.android.*;
import cn.wildfirechat.push.android.hms.newVersion.exception.HuaweiMesssagingException;
import cn.wildfirechat.push.android.hms.newVersion.message.AndroidConfig;
import cn.wildfirechat.push.android.hms.newVersion.message.Message;
import cn.wildfirechat.push.android.hms.newVersion.message.Notification;
import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiApp;
import cn.wildfirechat.push.android.hms.newVersion.messaging.HuaweiMessaging;
import cn.wildfirechat.push.android.hms.newVersion.model.Importance;
import cn.wildfirechat.push.android.hms.newVersion.model.Urgency;
import cn.wildfirechat.push.android.hms.newVersion.model.Visibility;
import cn.wildfirechat.push.android.hms.newVersion.reponse.SendResponse;
import cn.wildfirechat.push.android.hms.newVersion.util.InitAppUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

public class SendNotifyMessage2 {
    /**
     * send notification message
     *
     * @throws HuaweiMesssagingException
     */
    public void sendNotification(String appId, String appSecret, PushMessage pushMessage) throws HuaweiMesssagingException {
        HuaweiApp app = InitAppUtils.initializeApp(appId, appSecret);
        HuaweiMessaging huaweiMessaging = HuaweiMessaging.getInstance(app);

        Notification notification = Notification.builder()
                .setTitle(StringUtils.isEmpty(pushMessage.senderName) ? "收到一条消息" : pushMessage.senderName)
                .setBody(StringUtils.isEmpty(pushMessage.pushContent) ? JSONObject.toJSONString(pushMessage) : pushMessage.pushContent)
                .build();

//        JSONObject multiLangKey = new JSONObject();
//        JSONObject titleKey = new JSONObject();
//        titleKey.put("en","好友请求");
//        JSONObject bodyKey = new JSONObject();
//        titleKey.put("en","My name is %s, I am from %s.");
//        multiLangKey.put("key1", titleKey);
//        multiLangKey.put("key2", bodyKey);

        LightSettings lightSettings = LightSettings.builder().setColor(Color.builder().setAlpha(0f).setRed(0f).setBlue(1f).setGreen(1f).build())
                .setLightOnDuration("3.5")
                .setLightOffDuration("5S")
                .build();


        AndroidNotification androidNotification = AndroidNotification.builder()
                .setTitle(StringUtils.isEmpty(pushMessage.senderName) ? "收到一条消息" : pushMessage.senderName)
                .setBody(StringUtils.isEmpty(pushMessage.pushContent) ? JSONObject.toJSONString(pushMessage) : pushMessage.pushContent)
                .setDefaultSound(true)
                .setClickAction(ClickAction.builder().setType(3).build())
                .setImportance(Importance.HIGH.getValue())
                .setLightSettings(lightSettings)
                .setBadge(BadgeNotification.builder().setSetNum(1).setBadgeClass("com.easy.app.ui.splash.SplashActivity").build())
                .setVisibility(Visibility.PUBLIC.getValue())
                .build();

        AndroidConfig androidConfig = AndroidConfig.builder()
                .setUrgency(Urgency.HIGH.getValue())
                .setTtl((3 * 86400) + "s")
                .setNotification(androidNotification)
                .build();

        Message message = Message.builder()
                .setNotification(notification)
                .setAndroidConfig(androidConfig)
                .addToken(pushMessage.deviceToken)
                .build();

        SendResponse response = huaweiMessaging.sendMessage(message);
        HMSPush.LOG.info("通知栏消息推送response:"+ response.toString());
    }
}
